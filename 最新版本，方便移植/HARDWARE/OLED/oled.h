//////////////////////////////////////////////////////////////////////////////////	 
//
//  最近修改   : 2022-11-27
//  功能描述   : 0.96寸OLED 接口演示例程(STM32F103C8系列IIC)
//              说明:
//              ----------------------------------------------------------------
//              GND   电源地
//              VCC   接3.3v电源
//              SCL   接PA5（SCL）
//              SDA   接PA7（SDA）
//              ----------------------------------------------------------------
//  by:凝望夜空的心
//////////////////////////////////////////////////////////////////////////////////
#ifndef _OLED_H_
#define _OLED_H_

#include "sys.h"

#define Width		128
#define Height	64
#define Pages		8

//OLED指令
#define OLED_CMD  MYIIC_CMD		//写命令0
#define OLED_DATA MYIIC_DATA	//写数据1

//IIC驱动函数
void OLED_Write_IIC_Command(u8 IIC_Command);
void OLED_Write_IIC_Data(u8 IIC_Data);
void OLED_WR_Byte(u8 dat,u8 cmd);           //写入数据

void OLED_ShowPic(u8* pic);															//显示Pic

void OLED_Set_Pos(u8 x, u8 y);					//坐标设置
void OLED_Display_On(void);             //开启OLED显示  
void OLED_Display_Off(void);	   				//关闭OLED显示  
void OLED_Init(void);                   //初始化SSD1306
void OLED_Clear(void);                  //清屏函数
void OLED_Clear_page(u8 i);             //清页函数,清除第i(1<=i<=8)页
void OLED_SetBrightness(u8 brightness);	//设置屏幕亮度

#endif




