//////////////////////////////////////////////////////////////////////////////////	 
//
//  最近修改   : 2022-11-27
//  功能描述   : 0.96寸OLED 接口演示例程(STM32F103C8系列IIC)
//              说明: 
//              ----------------------------------------------------------------
//              GND   电源地
//              VCC   接3.3v电源
//              SCL   接PA5（SCL）
//              SDA   接PA7（SDA）
//              ----------------------------------------------------------------
//  by:凝望夜空的心
//////////////////////////////////////////////////////////////////////////////////

#include "oled.h"
#include "delay.h"
#include "myiic.h"

/**********************************************/
/****************** IIC驱动函数 ***************/
/**********************************************/
// IIC Write Command
void OLED_Write_IIC_Command(u8 IIC_Command)
{
	MYIIC_Start();
	MYIIC_Write_Byte(0x78);            //Slave address,SA0=0
	MYIIC_Wait_Ack();	
	MYIIC_Write_Byte(0x00);			//write command
	MYIIC_Wait_Ack();	
	MYIIC_Write_Byte(IIC_Command); 
	MYIIC_Wait_Ack();	
	MYIIC_Stop();
}
// IIC Write Data
void OLED_Write_IIC_Data(u8 IIC_Data)
{
	MYIIC_Start();
	MYIIC_Write_Byte(0x78);			//D/C#=0; R/W#=0
	MYIIC_Wait_Ack();	
	MYIIC_Write_Byte(0x40);			//write data
	MYIIC_Wait_Ack();	
	MYIIC_Write_Byte(IIC_Data);
	MYIIC_Wait_Ack();	
	MYIIC_Stop();
}
void OLED_WR_Byte(u8 dat, u8 cmd)
{
	if(cmd)	OLED_Write_IIC_Data(dat);
	else		OLED_Write_IIC_Command(dat);
}


/**********************************************/
/****************** OLED驱动函数 **************/
/**********************************************/
//坐标设置
void OLED_Set_Pos(u8 x, u8 y)
{
	OLED_WR_Byte(0xb0 + y, OLED_CMD);										//设置页地址（0~7）
	OLED_WR_Byte(((x & 0xf0) >> 4) | 0x10, OLED_CMD);	  //设置显示位置—列低地址
	OLED_WR_Byte((x & 0x0f), OLED_CMD); 								//设置显示位置—列高地址
}
//开启OLED显示    
void OLED_Display_On(void)
{
	OLED_WR_Byte(0X8D, OLED_CMD);  //SET DCDC命令
	OLED_WR_Byte(0X14, OLED_CMD);  //DCDC ON
	OLED_WR_Byte(0XAF, OLED_CMD);  //DISPLAY ON
}
//关闭OLED显示     
void OLED_Display_Off(void)
{
	OLED_WR_Byte(0X8D, OLED_CMD);  //SET DCDC命令
	OLED_WR_Byte(0X10, OLED_CMD);  //DCDC OFF
	OLED_WR_Byte(0XAE, OLED_CMD);  //DISPLAY OFF
}
//设置屏幕亮度
void OLED_SetBrightness(u8 brightness)
{
	OLED_WR_Byte(0x81,OLED_CMD);
	OLED_WR_Byte(brightness,OLED_CMD);
}
//清页函数
void OLED_Clear_page(u8 i)
{
	u8  n;
	OLED_WR_Byte (0xb0 + i - 1, OLED_CMD);  //设置页地址（0~7）
	OLED_WR_Byte (0x00, OLED_CMD);          //设置显示位置—列低地址
	OLED_WR_Byte (0x10, OLED_CMD);          //设置显示位置—列高地址   
	for(n = 0; n < Width; n++)
			OLED_WR_Byte(0, OLED_DATA);
}
/***************************************************
//清屏函数
***************************************************/
void OLED_Clear(void)  
{
	u8  i, n;
	for(i = 0; i < Pages; i++)  
	{  
		OLED_WR_Byte (0xb0 + i, OLED_CMD);  //设置页地址（0~7）
		OLED_WR_Byte (0x00, OLED_CMD);      //设置显示位置—列低地址
		OLED_WR_Byte (0x10, OLED_CMD);      //设置显示位置—列高地址   
		for(n = 0; n < Width; n++)
			OLED_WR_Byte(0, OLED_DATA); 
	} //更新显示
}

/***************************************************
//显示整个屏幕大小图片，二维数组pic
***************************************************/
void OLED_ShowPic(u8* pic)
{
	u8 i = 0;
	u8 page = 0;
	for(page = 0; page < Pages; page++)
	{
		OLED_Set_Pos(0, page);
		MYIIC_Start();
		MYIIC_Write_Byte(0x78);			//D/C#=0; R/W#=0
		MYIIC_Wait_Ack();
		MYIIC_Write_Byte(0x40);			//write data
		MYIIC_Wait_Ack();
		for(i = 0; i< Width; i++)
		{
			MYIIC_Write_Byte(pic[i]);
			MYIIC_Wait_Ack();
		}
		pic += Width;
		MYIIC_Stop();
	}
}

/***************************************************
//初始化SSD1306
***************************************************/
void OLED_Init(void)
{
//	MYIIC_InitGPIO();
	
  delay_ms(200);

  OLED_WR_Byte(0xAE,OLED_CMD);//关闭显示
	OLED_WR_Byte(0x00,OLED_CMD);//设置低列地址
	OLED_WR_Byte(0x10,OLED_CMD);//设置高列地址
	OLED_WR_Byte(0x40,OLED_CMD);//设置起始行地址  
	OLED_WR_Byte(0xB0,OLED_CMD);//设置页面地址
	OLED_WR_Byte(0x81,OLED_CMD);//控制管理
	OLED_WR_Byte(0xFF,OLED_CMD);//128
	OLED_WR_Byte(0xA1,OLED_CMD);//设置部分重映射
	OLED_WR_Byte(0xA6,OLED_CMD);//正常 / 镜像
	OLED_WR_Byte(0xA8,OLED_CMD);//设置多路复用率(1 to 64)
	OLED_WR_Byte(0x3F,OLED_CMD);//1/32 功率
	OLED_WR_Byte(0xC8,OLED_CMD);//Com扫描方向
	OLED_WR_Byte(0xD3,OLED_CMD);//设置显示offset
	OLED_WR_Byte(0x00,OLED_CMD);//
	
	OLED_WR_Byte(0xD5,OLED_CMD);//设置osc模块
	OLED_WR_Byte(0x80,OLED_CMD);//
	
	OLED_WR_Byte(0xD8,OLED_CMD);//将区域颜色模式设置为关闭
	OLED_WR_Byte(0x05,OLED_CMD);//
	
	OLED_WR_Byte(0xD9,OLED_CMD);//设置切换时间
	OLED_WR_Byte(0xF1,OLED_CMD);//
	
	OLED_WR_Byte(0xDA,OLED_CMD);//设置引脚配置
	OLED_WR_Byte(0x12,OLED_CMD);//
	
	OLED_WR_Byte(0xDB,OLED_CMD);//设置Vcomh
	OLED_WR_Byte(0x30,OLED_CMD);//
	
	OLED_WR_Byte(0x8D,OLED_CMD);//使能电荷泵
	OLED_WR_Byte(0x14,OLED_CMD);//
	
	OLED_WR_Byte(0xAF,OLED_CMD);//显示oled
}

