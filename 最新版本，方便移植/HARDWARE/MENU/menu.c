#include "menu.h"
#include "oled.h"
#include "string.h"

u8 MenuIndex = 0;
struct MENU* MenuBefore = NULL;
struct MENU* MenuHead = NULL;

//初始化菜单项
void MENU_InitMenu(struct MENU* menu, u8* name, u8 paraName, float* parameter, POINT position)
{
	menu->menuBef = NULL;
	menu->menuNext = NULL;
	menu->index = ++MenuIndex;
	menu->name = name;
	menu->paraName = paraName;
	menu->parameter = parameter;
	menu->position.x = position.x;
	menu->position.y = position.y;//+16*(menu->index-1);
	if(menu->index > 1)
	{
		menu->menuBef = MenuBefore;
		menu->menuNext = MenuHead;
		MenuBefore->menuNext = menu;
		MenuHead->menuBef = menu;
	}
	else	MenuHead = menu;
	MenuBefore = menu;
}
//设置菜单变量数值
void MENU_SetValue(u8 index, float value)
{
	struct MENU* menuTemp = MenuBefore;
	u8 i = MenuBefore->index;
	u8 ifend = False;
	
	if(MenuHead == NULL)	return;
	else if(MenuHead == MenuBefore)
	{
		if(index == MenuHead->index)
			*MenuHead->parameter = value;
		return;
	}
	
	while(ifend == False)
	{
		if(i < menuTemp->menuBef->index)	ifend = True;
		
		if(i == index)
		{
			*menuTemp->parameter = value;
			break;
		}
		
		menuTemp = menuTemp->menuBef;
		i = menuTemp->index;
	}
}
//显示菜单项
void MENU_ShowMenu(u8 size)
{
	struct MENU* menuTemp = MenuHead;
	u8 i = MenuHead->index;
	u8 index = 0;
	u8 ypose = 0;
	u8 ifend = False;
	
	if(size == SIZE16)	ypose = 16;
	else	ypose = 8;
	
	if(MenuHead == NULL)	return;
	else if(MenuHead == MenuBefore)
	{
		GUI_ShowString((int)MenuHead->position.x,(int)MenuHead->position.y,MenuHead->name,size,1);
		GUI_ShowChar((int)MenuHead->position.x+(size/2)*strlen(MenuHead->name)+4,(int)MenuHead->position.y,MenuHead->paraName, size, 1);
		GUI_ShowChar((int)MenuHead->position.x+(size/2)*strlen(MenuHead->name)+12,(int)MenuHead->position.y, ':', size, 1);
		GUI_ShowFloat((int)MenuHead->position.x+(size/2)*strlen(MenuHead->name)+24,(int)MenuHead->position.y,*MenuHead->parameter,4,size,1);
		return;
	}
	
	while(ifend == False)
	{
		if(i > menuTemp->menuNext->index)	ifend = True;
		
		GUI_ShowString((int)menuTemp->position.x,(int)MenuHead->position.y+ypose*index,menuTemp->name,size,1);
		GUI_ShowChar((int)menuTemp->position.x+(size/2)*strlen(menuTemp->name)+4,(int)MenuHead->position.y+ypose*index,menuTemp->paraName, size, 1);
		GUI_ShowChar((int)menuTemp->position.x+(size/2)*strlen(menuTemp->name)+12,(int)MenuHead->position.y+ypose*index, ':', size, 1);
		GUI_ShowFloat((int)menuTemp->position.x+(size/2)*strlen(menuTemp->name)+24,(int)MenuHead->position.y+ypose*index,*menuTemp->parameter,4,size,1);
		
		menuTemp = menuTemp->menuNext;
		i = menuTemp->index;
		index++;
	}
}
//移动菜单
void MENU_MoveMenu(int xstep, int ystep)
{
	struct MENU* menuTemp = MenuBefore;
	u8 i = MenuBefore->index;
	u8 ifend = False;
	
	if(MenuHead == NULL)	return;
	else if(MenuHead == MenuBefore)
	{
		MenuHead->position.x += xstep;
		MenuHead->position.y += ystep;
		return;
	}
	
	while(ifend == False)
	{
		if(i < menuTemp->menuBef->index)	ifend = True;
		
		menuTemp->position.x += xstep;
		menuTemp->position.y += ystep;
		
		menuTemp = menuTemp->menuBef;
		i = menuTemp->index;
	}
}

void MENU_DistoryMenu(u8 index)
{
	struct MENU* menuTemp = MenuBefore;
	u8 i = MenuBefore->index;
	u8 ifend = False;
	
	if(MenuHead == NULL)	return;
	else if(MenuHead == MenuBefore)
	{
		if(index == MenuHead->index)
		{
			MenuIndex = 0;
			MenuBefore = NULL;
			MenuHead = NULL;
		}
		return;
	}
	
	while(ifend == False)
	{
		if(i < menuTemp->menuBef->index)	ifend = True;
		
		if(i == index)
		{
			if(menuTemp == MenuHead)	MenuHead = menuTemp->menuNext;
			else if(menuTemp == MenuBefore)	MenuBefore = menuTemp->menuBef;
			menuTemp->menuBef->menuNext = menuTemp->menuNext;
			menuTemp->menuNext->menuBef = menuTemp->menuBef;
			break;
		}
		
		menuTemp = menuTemp->menuBef;
		i = menuTemp->index;
	}
}

void MENU_DistoryAllMenu(void)
{
	MenuIndex = 0;
	MenuBefore = NULL;
	MenuHead = NULL;
}
