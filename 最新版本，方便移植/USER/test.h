#ifndef _TEST_H_
#define _TEST_H_

#include "sys.h"

void TEST_ShowSence(void);
void TEST_ShowCar(int x, int y, float ang, u8 type);
void TEST_ShowWindmill(int x, int y, float ang);
void TEST_ShowLine(void);
void TEST_ShowMenu(void);
void TEST_ShowChinese(void);

#endif
