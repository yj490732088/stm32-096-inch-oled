#include "delay.h"
#include "sys.h"
#include "gui.h"
#include "oled.h"
#include "menu.h"
#include "test.h"
#include "bmp.h"
#include "myiic.h"
#include "mymath.h"
#include "stdlib.h"

void read(u8 addr);

int main(void)
{
	u8 i = 0;
	delay_init();	    	  //延时函数初始化
	MYIIC_InitGPIO();			//初始化软件IIC
	OLED_Init();			    //初始化OLED
	OLED_Clear();         //清屏
	GUI_ClearMap(0x00);
	
	GUI_ShowBMP(0, 0, 30, 32, PIG1, Normal);
	GUI_ShowBMP(40, 0, 70, 30, PIG2, Normal);
	GUI_ShowMap();
	while(i < 255)
	{
		i++;
		OLED_SetBrightness(i);
		delay_ms(10);
	}
	GUI_ClearMap(0x00);
	
	TEST_ShowLine();
	TEST_ShowChinese();
	TEST_ShowMenu();
	TEST_ShowSence();

}
