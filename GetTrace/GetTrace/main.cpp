#include<stdio.h>
#include<graphics.h>
#include<stdlib.h>

#define Width	128
#define Higth	64

#define U2D	0x01
#define D2U 0x02
#define L2R	0x03
#define R2L 0x04

#define FORECOLOR	RGB(0,150,150)
#define BACKCOLOR	RGB(150,150,0)
#define LBDOWNCOLOR	RGB(0,250,250)
#define RBDOWNCOLOR	RGB(250,250,0)
#define BUPCOLOR	RGB(0,250,0)

int size = 4;		//点宽
int winWidth = 10;	//窗口宽
int winHigth = 10;	//窗口高
int xpos = 0, ypos = 0;	//鼠标在点阵上坐标
char noMeaning = 0;		//无意义变量，用于获取按键延时，等待操作
POINT point;		//鼠标坐标
POINT target;		//位置
HWND hwnd = NULL;	//窗口句柄
FILE* file = NULL;	//文件指针
byte Map[Width][Higth] = { 0 };	//点阵
COLORREF cursorColor = BUPCOLOR;//光标颜色

void dealKey();		//处理按键消息
void putout();		//输出点阵
void putMap();		//输出坐标
void clearMap();	//清空点阵

int main()
{
	printf("输入点阵大小（宽,高）：");
	scanf_s("%d,%d", &winWidth, &winHigth);
	while(winWidth > 128 || winHigth > 64 || winWidth <= 0 || winHigth <= 0)
	{
		printf("点阵大小输入错误，应为0<宽<=128,0<高<=64，请重新输入：");
		scanf_s("%d,%d", &winWidth, &winHigth);
	}
	printf("\n默认取模方式为按列取模，从上向下、从左到右\n");
	if (winWidth > 30 || winHigth > 30)	size = 8;
	else size = (int)(240 / (winWidth) > 120 / winHigth ? 240 / winWidth : 120 / winHigth);
	errno_t error = fopen_s(&file, "点阵数组.txt", "at+");
	if (error == ENOENT)
	{
		if ((fopen_s(&file, "点阵数组.txt", "w+")) != 0)
		{
			printf("创建‘点阵数组.txt’时出错");
			noMeaning = getchar();
			exit(1);
		}
	}
	else if(error!=0)
	{
		printf("打开‘点阵数组.txt’时出错");
		noMeaning = getchar();
		exit(1);
	}
	Sleep(100);
	initgraph(size * winWidth, size * winHigth, 1);
	setbkcolor(BACKCOLOR);
	setfillcolor(FORECOLOR);
	cleardevice();
	putout();
	hwnd = GetHWnd();
	BeginBatchDraw();
	while (1)
	{
		dealKey();
		cleardevice();
		putout();
		FlushBatchDraw();
	}
	EndBatchDraw();
	fclose(file);
	closegraph();
	return 0;
}
void dealKey()		//处理按键消息
{
	ExMessage message;

	if (GetAsyncKeyState(VK_LBUTTON) & 0x8000)	//判断鼠标左键是否按下
	{
		Map[xpos][ypos] = 1;
		cursorColor = LBDOWNCOLOR;
	}
	else if (GetAsyncKeyState(VK_RBUTTON) & 0x8000)	//判断鼠标右键是否按下
	{
		Map[xpos][ypos] = 0;
		cursorColor = RBDOWNCOLOR;
	}
	else
		cursorColor = BUPCOLOR;

	message = getmessage(EM_MOUSE | EM_KEY);

	switch (message.message)
	{
	case WM_KEYDOWN:
		switch (message.vkcode)
		{
		case VK_RIGHT:
			if (target.x < winWidth - 1)	target.x += 1;
			break;
		case VK_LEFT:
			if (target.x > 0)	target.x -= 1;
			break;
		case VK_UP:
			if (target.y > 0)	target.y -= 1;
			break;
		case VK_DOWN:
			if (target.y < winHigth - 1)	target.y += 1;
			break;
		case VK_RETURN:
			putMap();
			break;
		case VK_DELETE:
			clearMap();
			break;
		case VK_SPACE:
			if (Map[target.x][target.y] == 0)	cursorColor = LBDOWNCOLOR;
			else	cursorColor = RBDOWNCOLOR;
			Map[target.x][target.y] = !Map[target.x][target.y];
			break;
		default:
			break;
		}
		break;
	case WM_MOUSEMOVE:
		xpos = message.x / size;
		ypos = message.y / size;
		target.x = xpos;
		target.y = ypos;
		if (xpos > winWidth)
			xpos = winWidth;
		else if (xpos < 0)
			xpos = 0;
		if (ypos > winHigth)
			ypos = winHigth;
		else if (ypos < 0)
			ypos = 0;
		break;
	default:
		break;
	}
}
void putout()		//输出点阵
{
	int i = 0, n = 0;
	for (i = 0; i < winWidth; i++)
	{
		for (n = 0; n < winHigth; n++)
		{
			if (i == target.x && n == target.y)
			{
				setfillcolor(cursorColor);
				fillrectangle(size * i, size * n, size * i + size, size * n + size);
				setfillcolor(FORECOLOR);
			}
			else if (Map[i][n] == 0)
			{
				rectangle(size * i, size * n, size * i + size, size * n + size);
			}
			else
			{
				fillrectangle(size * i, size * n, size * i + size, size * n + size);
			}
		}
	}
}
void putMap()		//输出坐标
{
	int i = 0, n = 0;
	int ymax = 0;
	char* str;
	if (winHigth % 8)
		ymax = (winHigth / 8) * 8 + 8;
	else
		ymax = winHigth;
	printf("{\n");
	fputs("{\n", file);
	for (i = 0; i < winWidth; i++)
	{
		for (n = 0; n < ymax; n++)
		{
			if (n % 8 == 0)
			{
				printf("B");
				fputs("B", file);
			}
			printf("%d", Map[i][n]);
			if (Map[i][n])
				fputs("1", file);
			else
				fputs("0", file);
			if (n % 8 == 7 && !(i + 1 == winWidth && n + 1 == ymax))
			{
				printf(",");
				fputs(",", file);
			}
		}
		printf("\n");
		fputs("\n", file);
	}
	printf("};\n");
	fputs("};\n", file);
	Sleep(100);
}
void clearMap()		//清空点阵
{
	int i = 0, n = 0;
	for (i = 0; i < winWidth; i++)
	{
		for (n = 0; n < winHigth; n++)
		{
			Map[i][n] = 0;
		}
	}
}