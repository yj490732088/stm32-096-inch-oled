#include "test.h"
#include "delay.h"
#include "sys.h"
#include "oled.h"
#include "menu.h"
#include "stdlib.h"
#include "binary.h"

	
const u8 Pic0[] = {
	B00000000,B00000000,B00000000,B00000000,
	B00000000,B00000000,B00000000,B00000000,
	B11110000,B00000000,B00000000,B00000000,
	B10001111,B00000000,B00000000,B00000000,
	B11000000,B11110000,B00000000,B00000000,
	B10100001,B00001111,B00000000,B00011100,
	B10010010,B00000100,B11110000,B00111110,
	B10001100,B00000011,B00111111,B11111111,
	B10001100,B00000011,B00111111,B11111111,
	B10010010,B00000100,B11110000,B00111110,
	B10100001,B00001111,B00000000,B00011100,
	B11000000,B11110000,B00000000,B00000000,
	B10001111,B00000000,B00000000,B00000000,
	B11110000,B00000000,B00000000,B00000000,
	B00000000,B00000000,B00000000,B00000000,
	B00000000,B00000000,B00000000,B00000000
};

const u8 Pic1[] = {
	B00000111,B11100000,B00000000,B00000000,
	B00000100,B10110000,B00000000,B00000000,
	B00000100,B11011000,B00000000,B00000000,
	B00000100,B01001100,B00000000,B00000000,
	B00000100,B01100100,B00000000,B00000000,
	B00000100,B00111100,B00000000,B00000000,
	B00000100,B00001110,B00000000,B00000000,
	B00000100,B00000011,B00000000,B00000000,
	B00000100,B00000011,B00000000,B00000000,
	B00000100,B00000011,B10000000,B00000000,
	B00000100,B00000010,B10000000,B00000000,
	B00000100,B00000011,B11000000,B00000000,
	B00000100,B00000001,B11100000,B00000000,
	B00000100,B00000000,B11110000,B00000000,
	B00000100,B00000000,B01111100,B00000000,
	B00000100,B00000000,B00001110,B00000000,
	B00000100,B00000000,B00000011,B00000000,
	B00000100,B11111111,B11110001,B00000000,
	B00000100,B11000011,B10011001,B00000000,
	B00000100,B10000001,B00001001,B10000000,
	B00000100,B10000001,B00001000,B10000000,
	B00000100,B10000001,B00001000,B10000000,
	B00000100,B10000001,B00001000,B10000000,
	B00000100,B10000001,B00001000,B10000000,
	B00000100,B11000011,B10011001,B10000000,
	B00000100,B11111111,B11110001,B00000000,
	B00000100,B00000000,B00000001,B00000000,
	B00000100,B00000000,B00000001,B00000000,
	B00000100,B00000000,B00000001,B00000000,
	B00000100,B00000001,B11111110,B00000000,
	B00000100,B00000011,B00000110,B00000000,
	B00000100,B00000100,B00001100,B00000000,
	B00000100,B00000100,B00111000,B00000000,
	B00000100,B00000100,B11100000,B00000000,
	B00000100,B00000111,B10000000,B00000000,
	B00000111,B11111000,B00000000,B00000000
};
const u8 Mask1[] = {
	B00000111,B11100000,B00000000,B00000000,
	B00000111,B11110000,B00000000,B00000000,
	B00000111,B11111000,B00000000,B00000000,
	B00000111,B11111100,B00000000,B00000000,
	B00000111,B11111100,B00000000,B00000000,
	B00000111,B11111100,B00000000,B00000000,
	B00000111,B11111110,B00000000,B00000000,
	B00000111,B11111111,B00000000,B00000000,
	B00000111,B11111111,B00000000,B00000000,
	B00000111,B11111111,B10000000,B00000000,
	B00000111,B11111111,B10000000,B00000000,
	B00000111,B11111111,B11000000,B00000000,
	B00000111,B11111111,B11100000,B00000000,
	B00000111,B11111111,B11110000,B00000000,
	B00000111,B11111111,B11111100,B00000000,
	B00000111,B11111111,B11111110,B00000000,
	B00000111,B11111111,B11111111,B00000000,
	B00000111,B11111111,B11111111,B00000000,
	B00000111,B11111111,B11111111,B00000000,
	B00000111,B11111111,B11111111,B10000000,
	B00000111,B11111111,B11111111,B10000000,
	B00000111,B11111111,B11111111,B10000000,
	B00000111,B11111111,B11111111,B10000000,
	B00000111,B11111111,B11111111,B10000000,
	B00000111,B11111111,B11111111,B10000000,
	B00000111,B11111111,B11111111,B00000000,
	B00000111,B11111111,B11111111,B00000000,
	B00000111,B11111111,B11111111,B00000000,
	B00000111,B11111111,B11111111,B00000000,
	B00000111,B11111111,B11111110,B00000000,
	B00000111,B11111111,B11111110,B00000000,
	B00000111,B11111111,B11111100,B00000000,
	B00000111,B11111111,B11111000,B00000000,
	B00000111,B11111111,B11100000,B00000000,
	B00000111,B11111111,B10000000,B00000000,
	B00000111,B11111000,B00000000,B00000000
};
const u8 Pic2[] = {
	B00011000,
	B00110100,
	B01001010,
	B10111011,
	B11011101,
	B01010010,
	B00101100,
	B00011000
};

const u8 Pic3[] = {
	B00000011,
	B00000011,
	B00000011,
	B00000011,
	B00000011,
	B00000111,
	B00001011,
	B00011111,
	B00101011,
	B00111111,
	B01010111,
	B01111111,
	B10101011,
	B11111111,
	B10101011,
	B01111111
};
	


void TEST_ShowSence(void)
{
	int i = 0;
	int x = 0, y = 0;
	u8 type = 0;
	
	for(x = 100, i = 0; x > 0; x--)
	{
		OLED_ClearMap(0x00);
		
		TEST_ShowWindmill(90-x,5,1080-3*i);
		TEST_ShowWindmill(20-x,15,720-2*i);
		
		i++;
		if(i == 360)	i = 0;
		
		OLED_ShowMap();
		delay_ms(20);
	}
	
	y = 32;
	x = -36;
	while(True)
	{
		OLED_ClearMap(0x00);
		
		TEST_ShowWindmill(90,5,1080-3*i);
		TEST_ShowWindmill(20,15,720-2*i);
		TEST_ShowCar(2*x,y-18,i,type);
		TEST_ShowCar(x,y,i,type);
		i++;
		if(i == 360)	i = 0;
		if(i%2 == 1)	x++;
		if(x > 128)
		{
			x = -36;
			type = !type;
		}
		
		OLED_ShowMap();
		delay_ms(20);
	}
}

void TEST_ShowCar(int x, int y, float ang, u8 type)
{
	POINT point;
	if(type)	OLED_ShowBMP(x,y,x+36,y+32,Pic1,Transparent|L2R|U2D);
	else			OLED_ShowPNG(x,y,x+36,y+32,Mask1,Pic1,L2R|U2D);
	OLED_SetSpanAng(ang);
	point = OLED_SetSpanCenter(x+4,y+23,x+8,y+27);
	OLED_ShowBMP(point.x,point.y,point.x+8,point.y+8,Pic2,Transparent|SpanSingle);
	point = OLED_SetSpanCenter(x+27,y+23,x+31,y+27);
	OLED_ShowBMP(point.x,point.y,point.x+8,point.y+8,Pic2,Transparent|SpanSingle);
}

void TEST_ShowWindmill(int x, int y, float ang)
{
	u8 n = 0;
	OLED_ShowBMP(x,y,x+16,y+32,Pic0,Transparent|U2D);
	for(n = 0; n < 4; n++)
	{
		OLED_SetSpanAng(ang+n*90);
		OLED_ShowBMP(x+8,y+3,x+8+16,y+3+8,Pic3,Transparent|SpanSingle|U2D);
	}
}

void TEST_ShowLine(void)
{
	u16 i = 0;
	OLED_ClearMap(0x00);
	for(i = 0; i <= 90; i += 5)
	{
		OLED_DrawLine(0,0,i,128,1);
		OLED_ShowMap();
		delay_ms(10);
	}
	for(i = 0; i <= 90; i += 5)
	{
		OLED_DrawLine(128,0,i+90,128,1);
		OLED_ShowMap();
		delay_ms(10);
	}
	for(i = 0; i <= 90; i += 5)
	{
		OLED_DrawLine(128,64,i+180,128,1);
		OLED_ShowMap();
		delay_ms(10);
	}
	for(i = 0; i <= 90; i += 5)
	{
		OLED_DrawLine(0,64,i+270,128,1);
		OLED_ShowMap();
		delay_ms(10);
	}
	delay_ms(500);
	delay_ms(500);
}

void TEST_ShowMenu(void)
{
	u8 num = 4;
	int index = 0;
	u8 n = 0;
	float p = 0, i = 0, d = 0, q = 0, r = 0, z = 0;
	struct MENU menu1,menu2,menu3,menu4,menu5,menu6;
	POINT point = {0, 0};
	
	MENU_InitMenu(&menu1,"menu1",'P', &p, point);
	MENU_InitMenu(&menu2,"menu2",'I', &i, point);
	MENU_InitMenu(&menu3,"menu3",'D', &d, point);
	MENU_InitMenu(&menu4,"menu4",'Q', &q, point);
	MENU_InitMenu(&menu5,"menu5",'R', &r, point);
	MENU_InitMenu(&menu6,"menu6",'Z', &z, point);

	while(True)
	{
		OLED_ClearMap(0x00);
		for(n = 1; n <= 3; n++)
		{
			MENU_SetValue(n,(float)index/(n+1)+(float)n/(index+1));
		}
		q = index/3;
		r = index/4.0;
		z = index;
		MENU_ShowMenu(SIZE16);
		if(index<32)
			MENU_MoveMenu(0,-1);
		else
			MENU_MoveMenu(0,1);
		index++;
		if(index >= 64)
		{
			if(num == 3)	break;
			MENU_DistoryMenu(num);
			num = num % 6 + 1;
			if(num == 1)	MENU_InitMenu(&menu5,"menu5",'I', &i, point);
			index = 0;
		}
		
    OLED_ShowMap();
		delay_ms(50);
	}
	MENU_DistoryAllMenu();
}

void TEST_ShowChinese(void)
{
	POINT point;
	u8 nums[6] = {0,1,2,3,4,5};
	int i = 0;
	u8 state = 0;
	u8 mode = Normal;
	
	mode = Transparent|SpanSingle;
	while(True)
	{
		if((mode&Transparent)&&(mode&Reverse))
		{
			OLED_ClearMap(0xff);
			OLED_ShowFloat(0,0,i-180,2,SIZE12,Reverse|Transparent);
		}
		else
		{
			OLED_ClearMap(0x00);
			OLED_ShowFloat(0,0,i-180,2,SIZE12,Normal);
		}
		
		OLED_SetSpanAng(i);
		if(mode&SpanSingle)
			point = OLED_SetSpanCenter(16,24,64,32);
		else
		{
			point.x = 16;
			point.y = 24;
		}
		
		OLED_ShowChineseStr(point.x,point.y,nums,6,mode);
		
		i+=2;
		if(i == 360)
		{
			i = 0;
			state++;
			if(state == 0)	mode = Transparent|SpanSingle;
			else if(state == 1)	mode = Transparent|Span;
			else if(state == 2)	mode = Transparent|SpanSingle|U2D|L2R|Reverse;
			else	break;
		}
		
		OLED_ShowMap();
		delay_ms(50);
	}
}


