#include "delay.h"
#include "sys.h"
#include "oled.h"
#include "menu.h"
#include "test.h"
#include "bmp.h"
#include "mymath.h"
#include "stdlib.h"

void read(u8 addr);

int main(void)
{
	delay_init();	    	  //延时函数初始化
	OLED_Init();			    //初始化OLED
	OLED_Clear();         //清屏
	OLED_ClearMap(0x00);
	
	OLED_ShowBMP(0, 0, 30, 32, PIG1, Normal);
	OLED_ShowBMP(40, 0, 70, 30, PIG2, Normal);
	OLED_ShowMap();
	delay_ms(1000);
	delay_ms(1000);
	delay_ms(1000);
	OLED_ClearMap(0x00);
	
	TEST_ShowLine();
	TEST_ShowChinese();
//	while(True)
//	{
//		read(0x70);
//		delay_ms(100);
//	}
	TEST_ShowMenu();
	TEST_ShowSence();

}


void read(u8 addr)
{
	//读取SHTC3温湿度传感器数据
	float Humidity,Temperature;
	u16 	tem,hum;
	
	OLED_ClearMap(0x00);
	MYIIC_Start();
	MYIIC_Write_Byte((addr<<1)+0x00);//写7位I2C设备地址加0作为写取位,1为读取位
	MYIIC_Wait_Ack();
	//发送0x3517唤醒芯片
	MYIIC_Write_Byte(0x35);
	MYIIC_Wait_Ack();
	MYIIC_Write_Byte(0x17);
	MYIIC_Wait_Ack();
	MYIIC_Stop();
	delay_ms(10);
	MYIIC_Start();
	MYIIC_Write_Byte((addr<<1)+0x00);//写7位I2C设备地址加0作为写取位,1为读取位
	MYIIC_Wait_Ack();
	//发送0x58e0，先读取湿度再读取温度
	MYIIC_Write_Byte(0x58);
	MYIIC_Wait_Ack();
	MYIIC_Write_Byte(0xe0);
	MYIIC_Wait_Ack();
	MYIIC_Stop();
	delay_ms(50);
	MYIIC_Start();
	MYIIC_Write_Byte((addr<<1)+0x01);//写7位I2C设备地址加0作为写取位,1为读取位
	
	if(MYIIC_Wait_Ack()==0)
	{
		hum = MYIIC_Read_Byte(ACK);
		hum = hum << 8;
		hum |= MYIIC_Read_Byte(ACK);
		MYIIC_Read_Byte(ACK);
		
		tem = MYIIC_Read_Byte(ACK);
		tem = tem << 8;
		tem |= MYIIC_Read_Byte(ACK);
		MYIIC_Read_Byte(NACK);
		MYIIC_Stop();
		
		MYIIC_Start();
		MYIIC_Write_Byte((addr<<1)+0x00);
		MYIIC_Wait_Ack();
		MYIIC_Write_Byte(0xb0);
		MYIIC_Wait_Ack();
		MYIIC_Write_Byte(0x98);
		MYIIC_Wait_Ack();
		MYIIC_Stop();
	}
	//printf("%d, %d, ",tem,hum);
	
	/*转换实际温度*/
	Temperature = (175.0 * ((float)tem) / 65535.0 - 45.0) ;// T = -45 + 175 * tem / (2^16-1)
	Humidity = (100.0 * ((float)hum) / 65535.0);// RH = hum*100 / (2^16-1)
	//printf(" T: %.5f, H: %.5f\r\n",Temperature,Humidity);
	OLED_ShowFloat(0, 8, Temperature, 5, SIZE16, Transparent);
	OLED_ShowFloat(0, 24, Humidity, 5, SIZE16, Transparent);
	OLED_ShowMap();
}
