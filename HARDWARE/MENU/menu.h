#ifndef _MENU_H_
#define _MENU_H_

#include "sys.h"
#include "oled.h"


//菜单结构体
struct MENU{
	u8 index;	//序号
	u8* name;	//菜单名
	u8 paraName;			//参数名
	float* parameter;	//参数
	POINT position;		//位置
	struct MENU* menuBef;		//下一个菜单位置
	struct MENU* menuNext;		//上一个菜单位置
};
//菜单初始化
void MENU_InitMenu(struct MENU* menu, u8* name, u8 paraName, float* parameter, POINT position);
void MENU_SetValue(u8 index, float value);
void MENU_ShowMenu(u8 size);
void MENU_MoveMenu(int xstep, int ystep);
void MENU_DistoryMenu(u8 index);
void MENU_DistoryAllMenu(void);

#endif

